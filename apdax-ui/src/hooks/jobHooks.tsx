import { HubConnection } from "@aspnet/signalr";
import {  useEffect } from "react";
import { IJob } from "../entities/job";

export function useJobSignal(methodName: string, conn: HubConnection, callback: any){

    function onProcessing(job: IJob){
        if(!job) throw new Error("Job is null or undefined");
        callback(job);
    }

    useEffect(() => {
        conn.on(
            methodName,
            (job) => onProcessing(job)
        );

        return () => {
            conn.off(methodName);
        }
    }, []);
}