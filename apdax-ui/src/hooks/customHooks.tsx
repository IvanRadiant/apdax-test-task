import { useState } from "react";

export function useFormInput<T>(initialValue: T, callback: any = null){
    const [value, setValue] = useState<T>(initialValue);

    function handleChange(e: any){
        setValue(e.target.value);
        callback && callback(e.target.value);
    }

    return {
        value,
        onChange: handleChange,
        setValue
    }
}

