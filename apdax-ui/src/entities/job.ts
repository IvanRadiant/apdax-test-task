import JobState from "./jobStatus";

export interface IJob {
    id: string;
    state: number;
    result: number; 
}

class Job implements IJob {
    id: string;
    state: number;
    result: number;

    constructor(id: string, state: JobState, result: number){
        this.id = id;
        this.state = state;
        this.result = result;
    }
}

export default Job;