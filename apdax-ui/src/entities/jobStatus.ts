enum JobState {
    Scheduled,
    InProcess,
    Completed,
    Failed
}

export default JobState;