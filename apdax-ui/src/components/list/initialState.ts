import { IJob } from "../../entities/job";

export interface IJobListState {
    jobs: IJob[]
}

const initialState : IJobListState = {
    jobs: []
}

export default initialState;