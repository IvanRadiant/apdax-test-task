import React from 'react'

import { IJob } from '../../entities/job'
import JobState from '../../entities/jobStatus';

export interface IJobItem {
    job: IJob;
}

const JobItem: React.FC<IJobItem> = (props) => {
    const job = props.job;

    
    function getTheme(){
        const state: JobState = job.state;
        
        switch(state){
            case JobState.Scheduled:
                return {
                    style: 'scheduled',
                    text: 'Waiting'
                };
            case JobState.Completed:
                return {
                    style: 'completed',
                    text: 'Completed'
                };
            case JobState.InProcess:
                return {
                    style: 'inprocess',
                    text: 'Processing'
                };
            case JobState.Failed:
                    return {
                        style: 'failed',
                        text: 'Failed'
                    };
            default:
                throw new Error('Unsupported state');
        }
    }

    const theme = getTheme();
    
    return (
        <div className={`job-item  ${theme.style}`}>
            {
                job.state === JobState.Completed ?
                    <div>{job.result}</div>
                    :
                    <div>{theme.text}</div>
            }
        </div>
    );
}

export default JobItem;