import React from 'react';
import useThunkReducer from "use-thunk-reducer"

import JobItem from './jobItem';
import { HubConnection } from '@aspnet/signalr';
import { useJobSignal } from '../../hooks/jobHooks';
import { IJob } from '../../entities/job';
import reducer from './reducer';
import { JOB_SCHEDULED, JOB_PROCESSING, JOB_COMPLETED } from './actionTypes';
import initialState from './initialState';

export interface IJobList {
    conn: HubConnection;
}

const JobList : React.FC<IJobList> = (props) => {
    const conn = props.conn;
    const [state, dispatch] = useThunkReducer(reducer, initialState);

    useJobSignal('scheduled', conn, (job: IJob) => {
        dispatch({
            type: JOB_SCHEDULED,
            job
        });
    });

    useJobSignal('processing', conn, (job: IJob) => {
        dispatch({
            type: JOB_PROCESSING,
            job
        });
    });

    useJobSignal('completed', conn, (job: IJob) => {
        dispatch({
            type: JOB_COMPLETED,
            job
        });
    });
    
    return (
        <div className="job-list">
            {state.jobs.map((job: IJob) => {
                return <JobItem key={job.id} job={job} />
            })}
        </div>
    );
}

export default JobList;