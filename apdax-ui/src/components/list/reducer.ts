import { 
    JOB_SCHEDULED,
    JOB_COMPLETED,
    JOB_PROCESSING,
    JOB_FAILED,
 } from './actionTypes'
import { IJobListState } from './initialState';

const reducer = (state : IJobListState, action: any) => {
    switch (action.type) {
        case JOB_SCHEDULED: {
            return {
                jobs: [...state.jobs, action.job]
            }
        }
        case JOB_PROCESSING:
        case JOB_COMPLETED:
            const newState = state.jobs.map(job => job.id === action.job.id ?
                { ...action.job }
                :
                job);

            return { 
                ...state,
                jobs:[ ...newState ]
            };
        case JOB_FAILED:
            return { 
                ...state,
                error: action.error
            };   
        default:
            return state;
    }
};

export default reducer;