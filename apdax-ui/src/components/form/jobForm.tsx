import React, { useState } from 'react'
import { HubConnection } from '@aspnet/signalr';

import { useFormInput } from '../../hooks/customHooks';

export interface IJobForm {
    conn: HubConnection;
}

const JobForm : React.FC<IJobForm> = (props) => {
    const conn = props.conn;
    const numb = useFormInput<number>(0);
    const [error, setError] = useState<string>();
      
    function sendJob(numb: number){
        conn.invoke("ScheduleTask", numb)
            .catch(err => setError(err.toString()));
    }

    function onSubmit(e: any){
        e.preventDefault();
        
        sendJob(numb.value);

        return false;
    }

    return (
        <form className='job-form' onSubmit={onSubmit}>
            <h4>Schedule a job</h4>

            <hr />

            <label>
                Number:
                <input type="number" min="0" max="2001" value={numb.value} onChange={numb.onChange} />
                {error && <p className="error">{error}</p>}
            </label>

            <button>Send job</button>
        </form>
    );
}

export default JobForm;