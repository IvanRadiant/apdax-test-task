import React, { useEffect } from 'react';
import { HubConnectionBuilder } from "@aspnet/signalr";

import './App.css';
import JobForm from './components/form/jobForm';
import JobList from './components/list/jobList';


const App: React.FC = () => {
  const conn =  new HubConnectionBuilder()
                    .withUrl("http://localhost:5000/jobsHub")
                    .build();

  useEffect(() =>{
    conn.start()
        .catch(err => console.error(err.toString()));
  }, []);

  return (
    <div className="app">
      <JobForm conn={conn} />
      <JobList conn={conn} />
    </div>
  );
}

export default App;
