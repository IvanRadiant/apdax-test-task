﻿using Apdax.Domain.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Apdax.Domain.Services
{
    public class CalculationService : ICalculationService
    {
        private readonly CalculationSettings _settings;

        public CalculationService(IOptions<CalculationSettings> settings)
        {
            _settings = settings?.Value ?? throw new ArgumentNullException(nameof(settings));
        }

        public int Calculate(int n)
        {
            Thread.Sleep(1);// slow it down a bit

            if(n > 1)
            {
                return (Calculate(n - 1) * (n + 1)) % _settings.P;
            }

            return 1;
        }

        public async Task<int> CalculateAsync(int n)
        {
            return await Task.Run(() => Calculate(n));
        }
    }
}
