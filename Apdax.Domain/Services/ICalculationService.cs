﻿using System.Threading.Tasks;

namespace Apdax.Domain.Services
{
    public interface ICalculationService
    {
        int Calculate(int n);
        Task<int> CalculateAsync(int n);
    }
}
