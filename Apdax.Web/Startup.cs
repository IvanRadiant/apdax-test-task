﻿using Apdax.BackgroundJobs.Configuration;
using Apdax.BackgroundJobs.interfaces;
using Apdax.BackgroundJobs.Jobs;
using Apdax.BackgroundJobs.Services;
using Apdax.BackgroundJobs.Storage;
using Apdax.Domain.Services;
using Apdax.Web.Application;
using Apdax.Web.Application.Services;
using Apdax.Web.Exceptions;
using Apdax.Web.Hubs;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;

namespace Apdax.Web
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IHubCallbackService<Job<int>>, HubCallbackService>();
            services.AddScoped<ICalculationService, CalculationService>();
            services.AddSingleton<IJobsStorage, JobsStorage>();
            services.AddSingleton<IHostedService, BackgroundJobsService>()
                    .Configure<BackgroundJobsSettings>(c => {
                        c.CheckUpdateTime = 1000;
                    });

            services.AddCors();
            services.AddSignalR();
        }

        public void Configure(IApplicationBuilder app, IServiceProvider serviceProvider)
        {
            app.UseExceptionHandler(errorApp =>
            {
                var logger = serviceProvider.GetRequiredService<ILogger<ExceptionManager>>();

                errorApp.Run(async context =>
                {
                    ExceptionManager.Manage(context, logger);
                });
            });
            app.UseCors(builder => 
            {
                builder.AllowAnyMethod()
                       .AllowAnyHeader()
                       .SetIsOriginAllowed(_ => true)//.AllowAnyOrigin() SignalR doesn't want work with AllowAnyOrigin
                       .AllowCredentials();
            });
            app.UseSignalR(routes =>
            {
                routes.MapHub<JobsHub>("/jobsHub");
            });
        }
    }
}
