﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Diagnostics;

namespace Apdax.Web.Exceptions
{
    public class ExceptionManager
    {
        public static void Manage(HttpContext httpContext, ILogger<ExceptionManager> logger)
        {
            var errorFeature = httpContext.Features.Get<IExceptionHandlerFeature>();
            var exception = errorFeature.Error;

            string errorDetail = exception.Demystify().ToString();

            logger.LogInformation(errorDetail);
        }

    }
}
