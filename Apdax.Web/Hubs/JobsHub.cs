﻿using Apdax.BackgroundJobs.interfaces;
using Apdax.BackgroundJobs.Jobs;
using Apdax.Domain.Services;
using Apdax.Web.Application;
using Apdax.Web.Application.Services;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace Apdax.Web.Hubs
{
    public class JobsHub : Hub
    {
        private readonly IHubCallbackService<Job<int>> _hubCallbackService;
        private readonly IJobsStorage _jobsStorage;
        private readonly ICalculationService _calculationService;

        public JobsHub(IJobsStorage jobsStorage,
                       IHubCallbackService<Job<int>> hubCallbackService,
                       ICalculationService calculationService)
        {
            _jobsStorage = jobsStorage;
            _hubCallbackService = hubCallbackService;
            _calculationService = calculationService;
        }

        public async Task ScheduleTask(int n)
        {
            if(n > 2000)
            {
                throw new ArgumentOutOfRangeException(nameof(n), "Value is out of range. Max value = 2000");
            }

            ApdaxJob job = new ApdaxJob(n,
                                        Context.ConnectionId,
                                        _hubCallbackService,
                                        _calculationService);

            _jobsStorage.ScheduleJob(job);
            
            await Clients.Caller.SendAsync("scheduled", job);
        }
    }
}
