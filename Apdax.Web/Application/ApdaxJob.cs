﻿using Apdax.BackgroundJobs.Jobs;
using Apdax.Domain.Services;
using Apdax.Web.Application.Services;
using System;

namespace Apdax.Web.Application
{
    public class ApdaxJob : Job<int>
    {
        private readonly int _n;
        private readonly string _connectionId;
        private readonly IHubCallbackService<Job<int>> _hubCallbackService;
        private readonly ICalculationService _calculationService;

        public ApdaxJob(int n,
                        string connectionId,
                        IHubCallbackService<Job<int>> hubCallbackService,
                        ICalculationService calculationService)
        {
            _n = n;
            _connectionId = connectionId ?? throw new ArgumentNullException(nameof(connectionId));
            _hubCallbackService = hubCallbackService ??
                                         throw new ArgumentNullException(nameof(hubCallbackService));
            _calculationService = calculationService ??
                                         throw new ArgumentNullException(nameof(calculationService));
        }

        protected override void Execute()
        {
            Result = _calculationService.Calculate(_n);
        }

        protected override void PreExecute()
        {
            State = JobState.InProcess;
            _hubCallbackService.NotifyAboutStartOfExecution(this, _connectionId);
        }

        protected override void PostExecute()
        {
            State = JobState.Completed;
            _hubCallbackService.NotifyAboutFinishOfExecution(this, _connectionId);
        }
    }
}
