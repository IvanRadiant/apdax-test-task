﻿using System.Threading.Tasks;

namespace Apdax.Web.Application.Services
{
    public interface IHubCallbackService<TJob>
    {
        Task NotifyAboutStartOfExecution(TJob job, string connectionId);
        Task NotifyAboutFinishOfExecution(TJob job, string connectionId);
    }
}
