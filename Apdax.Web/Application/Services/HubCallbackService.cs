﻿using Apdax.BackgroundJobs.Jobs;
using Apdax.Web.Hubs;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace Apdax.Web.Application.Services
{
    public class HubCallbackService : IHubCallbackService<Job<int>>
    {
        private readonly IHubContext<JobsHub> _hubContext;

        public HubCallbackService(IHubContext<JobsHub> hubContext)
        {
            _hubContext = hubContext;
        }

        public async Task NotifyAboutStartOfExecution(Job<int> job, string connectionId)
        {
            await _hubContext.Clients.Client(connectionId).SendAsync("processing", job);
        }

        public async Task NotifyAboutFinishOfExecution(Job<int> job, string connectionId)
        {
            await _hubContext.Clients.Client(connectionId).SendAsync("completed", job);
        }
    }
}
