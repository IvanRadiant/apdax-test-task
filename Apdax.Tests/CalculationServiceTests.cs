﻿using Apdax.Domain.Configuration;
using Apdax.Domain.Services;
using Microsoft.Extensions.Options;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace Apdax.Tests
{
    public class CalculationServiceTests
    {
        private readonly ICalculationService _calculationService;

        public CalculationServiceTests()
        {
            var mockOptions = new Mock<IOptions<CalculationSettings>>();
            mockOptions.Setup(o => o.Value).Returns(new CalculationSettings());

            _calculationService = new CalculationService(mockOptions.Object);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(100)]
        public void ShouldCalculateValue(int n)
        {
            //Arrange
            var res = int.MinValue;

            //Act
            res = _calculationService.Calculate(n);

            //Assert
            Assert.NotEqual(int.MinValue, res);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(100)]
        public async Task ShouldCalculateValueAsync(int n)
        {
            //Arrange
            var res = int.MinValue;

            //Act
            res = await _calculationService.CalculateAsync(n);

            //Assert
            Assert.NotEqual(int.MinValue, res);
        }
    }
}
