using Apdax.BackgroundJobs.interfaces;
using Apdax.BackgroundJobs.Jobs;
using Apdax.Domain.Services;
using Apdax.Web.Application.Services;
using Apdax.Web.Hubs;
using Microsoft.AspNetCore.SignalR;
using Moq;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Apdax.Tests
{
    public class JobsHubTests
    {
        [Fact]
        public async Task JobsHumScheduleNewTask()
        {
            //Arrange
            var mockJob = new Mock<Job<int>>();
            var mockClients = new Mock<IHubCallerClients>();
            var mockClientProxy = new Mock<IClientProxy>();
            var mockJobsStorage = new Mock<IJobsStorage>();
            var mockHubCallbackService = new Mock<IHubCallbackService<Job<int>>>();
            var calculationService = new Mock<ICalculationService>();
            var mockHubContext = new Mock<HubCallerContext>();

            mockClients.Setup(clients => clients.Caller).Returns(mockClientProxy.Object);

            calculationService.Setup(c => c.Calculate(10)).Returns(324);
            calculationService.Setup(c => c.CalculateAsync(10)).Returns(Task.FromResult(324));
            mockHubContext.Setup(c => c.ConnectionId).Returns("id");

            mockJobsStorage.Setup(s => s.ScheduleJob(mockJob.Object));

            JobsHub _jobsHub = new JobsHub(mockJobsStorage.Object,
                                           mockHubCallbackService.Object,
                                           calculationService.Object)
            {
                Clients = mockClients.Object,
                Context = mockHubContext.Object
            };

            //Act
            await _jobsHub.ScheduleTask(5);

            //Assert
            mockClients.Verify(clients => clients.Caller, Times.Once);

            mockClientProxy.Verify(
                clientProxy => clientProxy.SendCoreAsync(
                    "scheduled",
                    It.Is<object[]>(
                        o => o != null && o.Length == 1 && o[0] is IJob
                    ),
                    default),
                Times.Once);
        }

        [Fact]
        public async Task JobsHumThrowsOutOfRangeExceptionWhenSchedulingNewTask()
        {
            //Arrange
            var mockJob = new Mock<Job<int>>();
            var mockClients = new Mock<IHubCallerClients>();
            var mockClientProxy = new Mock<IClientProxy>();
            var mockJobsStorage = new Mock<IJobsStorage>();
            var mockHubCallbackService = new Mock<IHubCallbackService<Job<int>>>();
            var calculationService = new Mock<ICalculationService>();
            var mockHubContext = new Mock<HubCallerContext>();

            mockClients.Setup(clients => clients.Caller).Returns(mockClientProxy.Object);

            calculationService.Setup(c => c.Calculate(10)).Returns(324);
            calculationService.Setup(c => c.CalculateAsync(10)).Returns(Task.FromResult(324));
            mockHubContext.Setup(c => c.ConnectionId).Returns("id");

            mockJobsStorage.Setup(s => s.ScheduleJob(mockJob.Object));

            JobsHub _jobsHub = new JobsHub(mockJobsStorage.Object,
                                           mockHubCallbackService.Object,
                                           calculationService.Object)
            {
                Clients = mockClients.Object,
                Context = mockHubContext.Object
            };

            //Act-Assert
            await Assert.ThrowsAsync<ArgumentOutOfRangeException>(async () =>
            {
                await _jobsHub.ScheduleTask(50000);
            });
        }
    }
}
