﻿using Apdax.BackgroundJobs.interfaces;
using System;

namespace Apdax.BackgroundJobs.Jobs
{
    public abstract class Job<TResult> : IJob
    {
        /// <summary>
        /// Initilizes a new job
        /// </summary>
        public Job()
        {
            Id = Guid.NewGuid();
            State = JobState.Scheduled;
        }

        public Guid Id { get; }
        public JobState State { get; protected set; }
        public TResult Result { get; protected set; }

        void IJob.Execute()
        {
            PreExecute();
            Execute();
            PostExecute();
        }

        protected abstract void Execute();
        protected virtual void PreExecute() {}
        protected virtual void PostExecute() {}
    }
}
