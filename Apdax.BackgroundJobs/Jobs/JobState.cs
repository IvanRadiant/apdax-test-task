﻿namespace Apdax.BackgroundJobs.Jobs
{
    public enum JobState
    {
        Scheduled,
        InProcess,
        Completed,
        Failed
    }
}
