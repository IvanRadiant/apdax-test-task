﻿using Apdax.BackgroundJobs.Jobs;
using System;

namespace Apdax.BackgroundJobs.interfaces
{
    public interface IJob
    {
        Guid Id { get; }
        void Execute();
    }
}
