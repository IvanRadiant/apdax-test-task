﻿namespace Apdax.BackgroundJobs.interfaces
{
    public interface IJobsStorage
    {
        void ScheduleJob(IJob job);
        bool TryGetNextJob(out IJob job);
    }
}
