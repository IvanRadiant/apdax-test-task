﻿using System;
using System.Collections.Concurrent;
using Apdax.BackgroundJobs.interfaces;
using Microsoft.Extensions.Logging;

namespace Apdax.BackgroundJobs.Storage
{
    public class JobsStorage : IJobsStorage
    {
        private readonly ILogger<JobsStorage> _logger;
        private readonly ConcurrentQueue<IJob> _storage;

        public JobsStorage(ILogger<JobsStorage> logger)
        {
            _storage = new ConcurrentQueue<IJob>();
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public void ScheduleJob(IJob job)
        {
            _storage.Enqueue(job ?? throw new ArgumentNullException(nameof(job)));
            _logger.LogDebug($"{job.Id} Job has been enqueued to the storage.");
        }

        public bool TryGetNextJob(out IJob job)
        {
            var result = false;

            if(_storage.TryDequeue(out job))
            {
                result = true;
                _logger.LogDebug($"{job.Id} has been dequeued.");
            }

            _logger.LogDebug($"There are no jobs in the storage.");
            return result;
        }

        public IJob GetJob()
        {
            //TODO implement?
            return null;
        }

    }
}
