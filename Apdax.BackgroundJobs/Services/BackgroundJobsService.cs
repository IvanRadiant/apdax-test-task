﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Apdax.BackgroundJobs.Configuration;
using Apdax.BackgroundJobs.interfaces;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Apdax.BackgroundJobs.Services
{
    public class BackgroundJobsService : BackgroundService
    {
        private readonly IJobsStorage _jobStorageService;
        private readonly BackgroundJobsSettings _settings;
        private readonly ILogger<BackgroundJobsService> _logger;

        public BackgroundJobsService(
                                        IOptions<BackgroundJobsSettings> settings,
                                        ILogger<BackgroundJobsService> logger,
                                        IJobsStorage jobStorageService
                                    )
        {
            _settings = settings?.Value ?? throw new ArgumentNullException(nameof(settings));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _jobStorageService = jobStorageService ?? throw new ArgumentNullException(nameof(jobStorageService));
        }

        #region BackgroundService overriden memebers
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogDebug("BackgroundJobsPerformerService is starting.");

            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    _logger.LogDebug("BackgroundJobsPerformerService is doing background work.");
                    
                    if (_jobStorageService.TryGetNextJob(out IJob nextJob))
                    {
                        //I know about StackOverflowException and I can run it in a separate native Thread
                        //in order to provide it with its own stack. If it crashes, It doesn't matter.
                        //Thread th = new Thread(() => nextJob.Execute());
                        //but the task states that we need to process tasks in one thread.
                        nextJob.Execute();
                    }
                }
                catch(Exception e)
                {
                    _logger.LogError(e, e.Message);
                }

                await Task.Delay(_settings.CheckUpdateTime, stoppingToken);
            }

            _logger.LogDebug("BackgroundJobsPerformerService is stopping.");

            await Task.CompletedTask;
        }
        #endregion  
    }
}
